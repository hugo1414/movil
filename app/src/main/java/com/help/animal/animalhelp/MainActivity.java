package com.help.animal.animalhelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar =(Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    }


    public void entrar (View view){
        Intent entrar = new Intent(this ,PrincipalPantalla.class);
        startActivity(entrar);
    }
}
